# list

## status
- Main functionality implemented.
- Implemented polling worker, testing ...

## todo
- implement working using websock client, respond to events rather than using polling
- implement authorization checks via oidc groups and owner checks
- implement server settings and defaults
- implement all crud options around apikeys
- implement list-worker, stand-alone helm chart & optionally enabled via parent helm chart
- add prod pipeline to push images and charts to gitlab for public consumption

## recent
- merge separate apps into one
- switch from cockroachdb to yugabyte

## deployment
```
$ k -n list get all
NAME                              READY   STATUS    RESTARTS   AGE
pod/list-nats-0                   2/2     Running   0          4h21m
pod/list-server-8d58d9554-lbgq2   1/1     Running   0          3h9m
pod/yb-master-0                   3/3     Running   0          4h37m
pod/yb-master-1                   3/3     Running   0          4h37m
pod/yb-master-2                   3/3     Running   0          4h37m
pod/yb-tserver-0                  3/3     Running   0          4h37m
pod/yb-tserver-1                  3/3     Running   0          4h37m
pod/yb-tserver-2                  3/3     Running   0          4h37m

NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                                                                                AGE
service/list-nats              ClusterIP      10.105.226.16    <none>        4222/TCP                                                                               4h21m
service/list-nats-headless     ClusterIP      None             <none>        4222/TCP,8222/TCP                                                                      4h21m
service/list-server            ClusterIP      10.103.180.3     <none>        80/TCP                                                                                 4h37m
service/list-websocket         ClusterIP      10.99.61.53      <none>        80/TCP                                                                                 4h37m
service/yb-master-ui           LoadBalancer   10.108.195.23    10.4.0.76     7000:32130/TCP                                                                         4h37m
service/yb-masters             ClusterIP      None             <none>        7000/TCP,7100/TCP,15433/TCP                                                            4h37m
service/yb-tserver-service     LoadBalancer   10.105.123.204   10.4.0.77     6379:32737/TCP,9042:31154/TCP,5433:31497/TCP                                           4h37m
service/yb-tservers            ClusterIP      None             <none>        9000/TCP,12000/TCP,11000/TCP,13000/TCP,9100/TCP,6379/TCP,9042/TCP,5433/TCP,15433/TCP   4h37m
service/yugabyted-ui-service   LoadBalancer   10.105.23.164    10.4.0.75     15433:32573/TCP                                                                        4h37m
```
