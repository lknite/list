#!/bin/sh


# check current working directory
if [ ! -f "./$(basename $0)" ]; then
  echo "abort, change to scripts folder before executing"
  exit 1
fi

# check environment variables (in a /bin/sh compatible way)
read -r -d '' ITEMS <<EOF
PROJECT
REPO_CONTAINERS_USERNAME
REPO_CONTAINERS_PASSWORD
REPO_CONTAINERS_FQDN
EOF
for next in $ITEMS; do
  echo "checking for required env vars: $next"
  value=$(eval "echo \$$next")
  if [ -z $value ]; then
    echo "abort, required environment variable not present: '${next}'"
    exit 1
  fi
done

# tag version may be provided via environment variable or via ../tag-container file
if [ -z $TAG_VERSION ]; then
  # generate next tag version
  TAG_VERSION=`echo "$(cat ../build/ver-MAJOR).$(cat ../build/ver-MINOR).$(cat ../build/ver-PATCH)-$(echo $CI_PIPELINE_IID)"`
fi

# adjust folder to work out of
cd ../build

# copy over artifact
cp ../cmd/app app

# set TAG_VERSION inside container via the Dockerfile
sed -i "s/ENV VERSION.*/ENV VERSION ${TAG_VERSION}/g" ./Dockerfile

# set APP inside container via the Dockerfile
sed -i "s/ENV APP.*/ENV APP ${APP}/g" ./Dockerfile

# build image
echo docker build -t "${REPO_CONTAINERS_FQDN}/${PROJECT}/${APP}:${TAG_VERSION}" -t "${REPO_CONTAINERS_FQDN}/${PROJECT}/${APP}:latest" .
docker build -t "${REPO_CONTAINERS_FQDN}/${PROJECT}/${APP}:${TAG_VERSION}" -t "${REPO_CONTAINERS_FQDN}/${PROJECT}/${APP}:latest" .


# login to repo
echo docker login ${REPO_CONTAINERS_FQDN} -u "${REPO_CONTAINERS_USERNAME}" --password-stdin
echo ${REPO_CONTAINERS_PASSWORD} | docker login ${REPO_CONTAINERS_FQDN} -u "${REPO_CONTAINERS_USERNAME}" --password-stdin

# push to repo
echo docker push "${REPO_CONTAINERS_FQDN}/${PROJECT}/${APP}" --all-tags
docker push "${REPO_CONTAINERS_FQDN}/${PROJECT}/${APP}" --all-tags
