#!/bin/sh

# here for testing, but otherwise passed in via environment
#APISERVER=https://1.2.3.4:6443
#APITOKEN=$(cat ./token)
#NAMESPACE=kargo-list
#WAREHOUSE=warehouse

ATTR_NAME=kargo.akuity.io/refresh
ATTR_VALUE=$(date -Iseconds)

curl -sSk \
    -X PATCH \
    -d "{\"metadata\": {\"annotations\": {\"${ATTR_NAME}\": \"${ATTR_VALUE}\"} } } " \
    -H "Authorization: Bearer ${APITOKEN}" \
    -H 'Content-Type: application/merge-patch+json' \
    $APISERVER/apis/kargo.akuity.io/v1alpha1/namespaces/$NAMESPACE/warehouses/$WAREHOUSE
