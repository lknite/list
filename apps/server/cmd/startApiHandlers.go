package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func startApiHandlers(ctx context.Context) {
	//
	// api: health checks (no authentication)
	//

	log.Println("start handlers:")
	log.Println("- :8081 /healthz")
	HealthInit()

	//
	// api: websocket (authentication required via first message sent: 'Authorization' or 'API-KEY')
	//

	log.Println("- :8082 /ws")
	WebsocketInit()

	//
	// api: rest methods (authentication required via header: 'Authorization' or 'API-KEY')
	//

	mux := http.NewServeMux()

	log.Println("- :8080 /key")
	KeyInit(mux)
	log.Println("- :8080 /block")
	BlockInit(mux)
	log.Println("- :8080 /list")
	ListInit(mux)
	log.Println("- :8080 /server")
	ServerInit(mux)

	log.Println("start listener")

	// wrap entire mux with logger middleware
	wrappedMux := NewEnsureAuth(mux)

	// run via a goroutine
	wg.Add(1)
	go func() {
		defer wg.Done()

		// create a 'server' so we can later use 'shutdown'
		srv := &http.Server{
			Addr:    ":8080",
			Handler: wrappedMux,
		}

		// handle SIGINT / SIGTERM
		go func() {
			sigint := make(chan os.Signal)
			signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
			<-sigint

			log.Println("- shutdownApiHandlers")

			// cancel routine during shutdown, if needed
			sigctx, sigcancel := context.WithTimeout(context.Background(), 20*time.Second)
			defer func() {
				// extra handling here
				sigcancel()
			}()

			// stop listening, finish calls in progress, then exit
			if err := srv.Shutdown(sigctx); err != nil {
				log.Fatalf("- shutdownApiHandlers err: %+v", err)
			}
			log.Print("- shutdownApiHandlers exited")
		}()

		// begin handling connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
}
