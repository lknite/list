package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/lknite/list/internal/model"

	"github.com/golang-jwt/jwt/v5"
)

func ListInit(mux *http.ServeMux) {

	ListInitHandles(mux)
}

func ListInitHandles(mux *http.ServeMux) {

	// implementation is here, rather than in controller because we would
	// otherwise end up sending a very large amount of data twice
	mux.HandleFunc("/list/big", func(w http.ResponseWriter, r *http.Request) {
		// get the authenticated user claims from the request context
		claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
		log.Println("claims pass-through: " + fmt.Sprintf("%v", claims["email"]))

		// -- handle request

		switch r.Method {
		case http.MethodGet:
			// Get a record
			log.Println("[get] /list/big")

			// access parameters passed in via the url
			values := r.URL.Query()
			if !values.Has("id") {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte("list id must be specified as url parameter"))
				return
			}
			/*
				if !values.Has("key") {
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("list key must be specified as url parameter"))
					return
				}
			*/
			if !values.Has("type") {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte("list type must be specified as url parameter, either type=total or type=size"))
				return
			}

			/**
			 * Load up demo bignum
			 */

			// get context to use with transaction
			ctx := r.Context()

			// get bignum meta data
			var bigId int
			if values.Get("type") == "total" {
				log.Printf("type: %v\n", values.Get("type"))
				id, err := strconv.Atoi(values.Get("id"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("error: converting id"))
					return
				}
				log.Printf("id: %v\n", values.Get("id"))
				/*
					key, err := uuid.Parse(values.Get("key"))
					if err != nil {
						log.Println(err)
						w.WriteHeader(http.StatusNotFound)
						w.Write([]byte("error: converting key"))
						return
					}
				*/
				err = db.QueryRowContext(ctx, `SELECT "totalId" FROM list WHERE "id"=$1`, id).Scan(&bigId)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("invalid id/type combination"))
					return
				}
			} else {
				log.Printf("type: %v\n", values.Get("type"))
				id, err := strconv.Atoi(values.Get("id"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("error: converting id"))
					return
				}
				log.Printf("id: %v\n", values.Get("id"))
				/*
					key, err := uuid.Parse(values.Get("key"))
					if err != nil {
						log.Println(err)
						w.WriteHeader(http.StatusNotFound)
						w.Write([]byte("error: converting key"))
						return
					}
				*/
				err = db.QueryRowContext(ctx, `SELECT "sizeId" FROM list WHERE "id"=$1`, id).Scan(&bigId)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("invalid id/type combination"))
					return
				}
			}

			// get big record
			var big model.Big
			//log.Println("bbb")
			err := db.QueryRowContext(ctx, `SELECT "n", "m", "r" FROM big WHERE "id"=$1`, bigId).Scan(&big.N, &big.M, &big.R)
			if err != nil {
				log.Println(err)
				return
			}

			// get big parts
			//log.Println("ccc")
			rows, err := db.QueryContext(ctx, `SELECT "index", "part", "hash" FROM big_part WHERE "bigId"=$1;`, bigId)
			if err != nil {
				log.Println(err)
				return
			}
			defer rows.Close()
			for rows.Next() {
				var p model.BigPart
				//log.Println("ddd")
				err = rows.Scan(&p.Index, &p.Part, &p.Hash)
				if err != nil {
					log.Println(err)
					return
				}

				big.Parts = append(big.Parts, p)

				// send a part at a time rather than all at once
				w.Write([]byte(p.Part))
			}

			/*
				// return the big (we only allow this once as a ddos prevention measure)
				bytes, err := json.Marshal(big)
				if err != nil {
					log.Println(err)
					return
				}
				w.Write(bytes)
			*/
		}
	})

	mux.HandleFunc("/list", func(w http.ResponseWriter, r *http.Request) {
		// get the authenticated user claims from the request context
		claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
		log.Println("claims pass-through: " + fmt.Sprintf("%v", claims["email"]))

		// -- handle request

		switch r.Method {
		case http.MethodGet:
			/**
			 * get list(s)
			 */
			log.Println("[get] /list")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("list.get", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			w.Write(m.Data)

		case http.MethodPost:
			/**
			 * create new list
			 */
			log.Println("[post] /list")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("list.post", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		case http.MethodPut:
			/**
			 * update list
			 */
			log.Println("[put] /list")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("list.put", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		case http.MethodDelete:
			/**
			 * delete list
			 */
			log.Println("[delete] /list")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("list.delete", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		}
	})
}
