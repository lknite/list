package main

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"strings"

	"log"

	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/model"
)

func serverPost(m *nats.Msg) {
	// log
	log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	// ***
	// * BEGIN
	// *

	// apiKey consists of:
	// <email>:<password>
	// and is stored as a rsa256 hash

	// generate password to use with the apiKey
	password := generatePassword()

	// generate apikey by merging email and password
	key := request.Who + ":" + password
	log.Printf("key: %v\n", key)

	// get hash to store in database
	hash := sha256.Sum256([]byte(key))
	hashb64 := base64.StdEncoding.EncodeToString([]byte(hash[:]))
	log.Printf("hash: %v\n", hash)
	log.Printf("hashstr: %v\n", hashb64)

	// convert claims to json
	claimsJson, _ := json.Marshal(request.Claims)

	// for aes the 'secret' must be 32 bytes (a guid is 32 if you take out the '-')
	secretKey := strings.Join(strings.Split(password, "-"), "")

	// encrypt claims using password
	claims := encrypt(secretKey, string(claimsJson))
	claimsb64 := base64.StdEncoding.EncodeToString([]byte(claims))
	log.Printf("claims aes and base64 encrypted: %v\n", claimsb64)

	// create a new token record in database
	var tokenId int
	err = tx.QueryRowContext(ctx, `INSERT INTO key ("who", "hash", "exp", "name", "claims") VALUES ($1, $2, 0, $3, $4) RETURNING id;`,
		request.Who, hashb64, request.Values.Get("name"), claimsb64).Scan(&tokenId)
	if err != nil {
		log.Println(err)
		return
	}
	// Return id of new token
	log.Printf("tokenId: %d\n", tokenId)

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return
	}

	//
	type Data struct {
		Key string `json:"key"`
	}
	var data Data

	//
	data.Key = key

	// respond to message with result
	j, _ := json.Marshal(data)
	m.Respond(j)
}
