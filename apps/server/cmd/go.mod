module gitlab.com/lknite/list/apps/app

go 1.22.1

require (
	github.com/MicahParks/keyfunc/v3 v3.3.3
	github.com/gobwas/ws v1.4.0
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/google/uuid v1.6.0
	github.com/lib/pq v1.10.9
	github.com/nats-io/nats.go v1.35.0
	gitlab.com/lknite/list/internal/dbbig v0.0.0-unpublished
	gitlab.com/lknite/list/internal/model v0.0.0-unpublished
)

require (
	github.com/MicahParks/jwkset v0.5.18 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/time v0.5.0 // indirect
)

replace gitlab.com/lknite/list/internal/model v0.0.0-unpublished => ../../../internal/model

replace gitlab.com/lknite/list/internal/dbbig v0.0.0-unpublished => ../../../internal/dbbig
