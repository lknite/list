package main

import (
	"context"
	"encoding/json"
	"log"

	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/dbbig"
	"gitlab.com/lknite/list/internal/model"
)

func listDelete(m *nats.Msg) {
	// log
	//log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	log.Println("[delete /list] name: ", request.Values.Get("name"))

	// get lock on list
	var list model.ListRef
	err = tx.QueryRowContext(ctx, "SELECT * FROM list WHERE name=$1 AND state=$2 OR state=$3 FOR UPDATE;", request.Values.Get("name"), "complete", "suspended").Scan(
		&list.Id, &list.When, &list.Who, &list.Name, &list.Task, &list.Action, &list.State, &list.Access, &list.TotalId, &list.SizeId)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("list found: %d", list.Id)

	// get lock on root block
	var block model.BlockRef
	err = tx.QueryRowContext(ctx, `SELECT "id","indexId","sizeId" FROM block WHERE "list"=$1 FOR UPDATE;`, list.Id).Scan(
		&block.Id, &block.IndexId, &block.SizeId)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("list root block: %d", block.Id)

	// delete bigs associated with block
	dbIndex, err := dbbig.Load(db, block.IndexId)
	dbSize, err := dbbig.Load(db, block.SizeId)

	// delete bigs 'index' & 'size' associated with block
	// todo: fix, this loads the big, which is unnecessary just to do the delete
	dbIndex.Delete()
	dbSize.Delete()

	// delete 'root' block
	_, err = tx.ExecContext(ctx, "DELETE FROM block WHERE id=$1;", block.Id)

	// (again) to also delete the remaining bigs associated with the 'complete' block
	err = tx.QueryRowContext(ctx, `SELECT "id","indexId","sizeId" FROM block WHERE "list"=$1 FOR UPDATE;`, list.Id).Scan(
		&block.Id, &block.IndexId, &block.SizeId)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("list complete block: %d", block.Id)

	// delete bigs associated with block
	dbIndex, err = dbbig.Load(db, block.IndexId)
	dbSize, err = dbbig.Load(db, block.SizeId)

	// delete bigs 'index' & 'size' associated with block
	// todo: fix, this loads the big, which is unnecessary just to do the delete
	dbIndex.Delete()
	dbSize.Delete()

	// delete 'complete' block
	_, err = tx.ExecContext(ctx, "DELETE FROM block WHERE id=$1;", block.Id)

	// delete groups associated with list

	// delete bigs associated with list
	dbTotal, err := dbbig.Load(db, list.TotalId)
	//dbSize, err = dbbig.Load(db, list.SizeId)

	// delete bigs 'index' & 'size' associated with block
	// todo: fix, this loads the big, which is unnecessary just to do the delete
	dbTotal.Delete()
	//dbSize.Delete()

	// delete list
	_, err = tx.ExecContext(ctx, "DELETE FROM list WHERE name=$1 AND state=$2 OR state=$3;", request.Values.Get("name"), "complete", "suspended")

	// Commit the transaction.
	log.Printf("commiting")
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return
	}

	// send to listeners
	log.Printf("send to listeners (websocket), todo")
	//nc.Publish("list.delete", []byte("{ \"list.delete\": \""+list.Name+"\" }"))

	// respond to message with result
	m.Respond([]byte("ok"))
}
