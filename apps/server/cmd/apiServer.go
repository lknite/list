package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/lknite/list/internal/model"

	"github.com/golang-jwt/jwt/v5"
)

func ServerInit(mux *http.ServeMux) {

	ServerInitHandles(mux)
}

func ServerInitHandles(mux *http.ServeMux) {

	mux.HandleFunc("/server", func(w http.ResponseWriter, r *http.Request) {
		// get the authenticated user claims from the request context
		claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
		log.Println("claims pass-through: " + fmt.Sprintf("%v", claims["email"]))

		// -- handle request

		switch r.Method {
		case http.MethodGet:
			/**
			 * get server attribute(s)
			 */
			log.Println("[get] /server")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("server.get", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			w.Write(m.Data)

		case http.MethodPost:
			/**
			 * create new server attribute(s)
			 */
			log.Println("[post] /server")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("server.post", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		case http.MethodPut:
			/**
			 * update server attribute(s)
			 */
			log.Println("[put] /server")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("server.put", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		case http.MethodDelete:
			/**
			 * delete server attribute(s)
			 */
			log.Println("[delete] /server")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("server.delete", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		}
	})
}
