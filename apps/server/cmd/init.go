package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/MicahParks/keyfunc/v3"
	"github.com/nats-io/nats.go"
)

/*
func init() {
	// init log
	initLog()

	// display container version
	log.Printf("version: %v", os.Getenv("VERSION"))

	// init everything else
	initDb()
	initDbTables()
	initNats()
	initJwks()
	checkDependencies()
}
*/

func initLog() {
	// parse off exec name
	binary := os.Args[0]
	parts := strings.Split(os.Args[0], "/")
	if len(parts) > 1 {
		binary = parts[len(parts)-1]
	}

	// initialize log
	_ = binary
	//log.SetPrefix(binary + ": ")
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)
}

// init connection to database
func initDb() {
	/*
		// display env vars
		fmt.Println()
		for _, e := range os.Environ() {
			fmt.Println(e)
			//pair := strings.SplitN(e, "=", 2)
			//fmt.Println(pair[0])
		}
	*/

	// yugabyte
	svcHost, err := getServiceHost("YB_TSERVER_SERVICE")
	if err != nil {
		log.Fatalln("restart, postgres server does not yet exist (1)")
	}
	svcPort, err := getService("YB_TSERVER_SERVICE", "_SERVICE_PORT_TCP_YSQL_PORT")
	if err != nil {
		log.Fatalln("restart, postgres server does not yet exist (2)")
	}
	log.Println("yugabyte detected")

	/*
		// cockroachdb
		svcHost, err = getServiceHost("cockroachdb_public")
		if err != nil {
			log.Fatalln("restart, postgres server does not yet exist")
		}
		svcPort, err = getService("cockroachdb_public", "_SERVICE_PORT_SQL")
		if err != nil {
			log.Fatalln("restart, postgres server does not yet exist")
		}
		log.Println("cockroachdb detected")
	*/

	// yugabyte
	//connection := fmt.Sprintf("postgresql://%v:%v@%v:%v?sslmode=require",
	connection := fmt.Sprintf("postgresql://%v:%v@%v:%v/%v", //?load_balance=true&yb_servers_refresh_interval=240",
		os.Getenv("POSTGRES_USERNAME"),
		os.Getenv("POSTGRES_PASSWORD"),
		svcHost, svcPort, os.Getenv("DB"))
	log.Println("debug: db connection: " + connection)

	/* cockroachdb
	connection := fmt.Sprint(
		" host="+svcHost,
		" port="+svcPort,
		" sslmode=require",
		" sslrootcert=/cockroach/cockroach-certs/ca.crt",
		" sslkey=/cockroach/cockroach-certs/client.root.key",
		" sslcert=/cockroach/cockroach-certs/client.root.crt",
	)
	*/

	//log.Println("debug: db connection: " + connection)

	// db connection
	for {
		// get connection to database
		var err error
		db, err = sql.Open("postgres", connection)
		if err != nil {
			time.Sleep(1000)

			// try again
			continue
		}

		break
	}

	// wait for database to become ready
	log.Println("wait for database to become ready ...")
	for {
		/*
			// check db endpoint for ready status
			svcHttpPort, err := getService("cockroachdb_public", "_SERVICE_PORT_HTTP")
			if err != nil {
				log.Fatalln("restart, postgres server does not yet exist")
			}
			// initialize rest client
			client := &http.Client{
				Timeout: time.Second * 10,
			}
			log.Printf("svcHost: %v\n", svcHost)
			log.Printf("svcPort: %v\n", svcHttpPort)
			uri := fmt.Sprintf("http://%v:%v/health", svcHost, svcHttpPort)
			req, err := http.NewRequest(http.MethodGet, uri, nil)
			if err != nil {
				log.Fatal(err)
			}

			// invoke method
			resp, err := client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()

			// if all done, break
			if resp.StatusCode == 200 {
				break
			}

			// otherwise pause a second then retry
			time.Sleep(1000)
		*/

		// db query test
		rows, err := db.Query(`SELECT datname FROM pg_database`)
		//rows, err := db.Query(`SELECT * FROM pg_catalog.pg_tables WHERE schemaname = 'information_schema'`)
		if err != nil {
			log.Printf("wait for db to become ready err: %v\n", err)
			time.Sleep(1000)

			// try again
			continue
		}
		defer rows.Close()
		break
	}

	log.Println("create database:")
	sqlDb()

	/*
		// update connect string with database specified
		//connStr = fmt.Sprintf("postgresql://%v:%v@%v:%v/%v"+
		connStr := fmt.Sprintf("postgresql://%v:%v/%v"+
			//"?sslmode=verify-full"+
			"?sslmode=require"+
			"&sslrootcert="+url.QueryEscape("/cockroach/cockroach-certs/ca.crt")+
			"&sslkey="+url.QueryEscape("/cockroach/cockroach-certs/client.root.key")+
			"&sslcert="+url.QueryEscape("/cockroach/cockroach-certs/client.root.crt"),
			//os.Getenv("POSTGRES_USERNAME"),
			//os.Getenv("POSTGRES_PASSWORD"),
			svcHost, svcPort,
			os.Getenv("DB"))
		log.Println("debug: db connStr: " + connStr)
	*/

	// add database to connection string
	// yugabyte
	connection = fmt.Sprint(
		" host="+svcHost,
		" port="+svcPort,
		" dbname="+os.Getenv("DB"),
		" user="+os.Getenv("POSTGRES_USERNAME"),
		" password="+os.Getenv("POSTGRES_PASSWORD"),
	)
	log.Println("debug: db connection: " + connection)

	//connection = fmt.Sprintf("postgresql://%v:%v@%v:%v/yugabyte", //?load_balance=true&yb_servers_refresh_interval=240",
	//	os.Getenv("POSTGRES_USERNAME"),
	//	os.Getenv("POSTGRES_PASSWORD"),
	//	svcHost, svcPort)

	/*
		// cockroachdb
		connection = fmt.Sprint(
			" host="+svcHost,
			" port="+svcPort,
			" dbname=lido",
			" sslmode=require",
			" sslrootcert=/cockroach/cockroach-certs/ca.crt",
			" sslkey=/cockroach/cockroach-certs/client.root.key",
			" sslcert=/cockroach/cockroach-certs/client.root.crt",
		)
	*/

	//log.Println("debug: db connStr: " + connStr)
	log.Println("debug: db connection: " + connection)

	// db connection
	for {
		// get connection to database
		var err error
		db, err = sql.Open("postgres", connection)
		if err != nil {
			time.Sleep(1000)

			// try again
			continue
		}

		break
	}

	/*
		log.Println("select database:")
		_, err = db.Exec(`\c ` + os.Getenv("DB"))
		if err != nil {
			log.Printf("err: %v\n", err)
			log.Panicf("unable to select database")
		}
	*/
}

func initDbTables() {

	log.Println("create tables:")

	// copied over
	sqlAuth()
	sqlBig()
	sqlBlock()
	sqlDb()
	sqlGroup()
	sqlList()
	sqlServer()
}

func getService(service string, pattern string) (string, error) {
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		if strings.Contains(pair[0], strings.ToUpper(service)) &&
			strings.Contains(pair[0], strings.ToUpper(pattern)) {
			return pair[1], nil
		}
	}
	return "", errors.New("getServiceHost(1) '" + service + "' not found")
}

func getServiceHost(service string) (string, error) {
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		if strings.HasSuffix(pair[0], strings.ToUpper(service)+"_SERVICE_HOST") {
			return pair[1], nil
		}
	}
	return "", errors.New("getServiceHost(2) '" + service + "' not found")
}

func getServicePort(service string) (int, error) {
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		if strings.HasSuffix(pair[0], strings.ToUpper(service)+"_SERVICE_PORT") {
			i, err := strconv.Atoi(pair[1])
			if err != nil {
				log.Println(err)
			}
			return i, nil
		}
	}
	return -1, errors.New("getServiceHost(3) '" + service + "' not found")
}

// init connection to nats
func initNats() {
	// get 'nats' connect string via the environment
	svcHost, err := getServiceHost("nats")
	if err != nil {
		log.Println(err)
	}
	svcPort, err := getServicePort("nats")
	if err != nil {
		log.Println(err)
	}
	connStr := fmt.Sprintf("nats://%v:%d", svcHost, svcPort)

	//
	for {
		// connect with nats
		nc, err = nats.Connect(connStr)
		if err != nil {
			time.Sleep(1000)

			// try again
			continue
		}

		break
	}
}

func initJwks() {
	/**
	 * acquire jwks uri via rfc8414: begin
	 */

	// initialize rest client
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	uri := os.Getenv("OIDC_PROVIDER_URI") + "/.well-known/openid-configuration"
	log.Printf("uri: %v", uri)
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		log.Fatal(err)
	}

	// invoke method
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	//
	if resp.StatusCode != 200 {
		log.Fatal("jwks, StatusCode: " + strconv.Itoa(resp.StatusCode))
	}

	// read off body of response
	respBody, _ := io.ReadAll(resp.Body)

	//
	type Rfc8414 struct {
		Jwks_uri string `json:"jwks_uri"`
	}
	var oidc Rfc8414

	// parse json, acquire jwks_uri
	json.Unmarshal(respBody, &oidc)
	log.Printf("oidc.Jwks_uri: %v", oidc.Jwks_uri)

	/**
	 * acquire jwks uri via rfc8414: end
	 */

	//
	jwks, err = keyfunc.NewDefault([]string{oidc.Jwks_uri})
	if err != nil {
		log.Fatalf("Failed to create client JWK set. Error: %s", err)
	}
}

/*
// required to proceed
func checkDependencies() {
	// db query test
	for {
		// db query test
		rows, err := db.Query(`SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'`)
		if err != nil {
			time.Sleep(1000)

			// try again
			continue
		}
		defer rows.Close()

		break
	}
}
*/
