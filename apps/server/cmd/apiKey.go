package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/lknite/list/internal/model"

	"github.com/golang-jwt/jwt/v5"
)

func KeyInit(mux *http.ServeMux) {

	KeyInitHandles(mux)
}

func KeyInitHandles(mux *http.ServeMux) {

	mux.HandleFunc("/key", func(w http.ResponseWriter, r *http.Request) {
		// get the authenticated user claims from the request context
		claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
		log.Println("claims pass-through: " + fmt.Sprintf("%v", claims["email"]))

		// -- handle request

		switch r.Method {
		case http.MethodGet:
			/**
			 * get key(s)
			 */
			log.Println("[get] /key")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("key.get", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			w.Write(m.Data)

		case http.MethodPost:
			/**
			 * create new key
			 */
			log.Println("[post] /key")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("key.post", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)

		case http.MethodPut:
			// Update
		case http.MethodDelete:
			/**
			 * delete new key(s)
			 */
			log.Println("[delete] /key")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("key.delete", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			//log.Println(string(m.Data))
			w.Write(m.Data)
		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		}
	})
}
