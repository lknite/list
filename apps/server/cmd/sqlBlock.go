package main

import (
	"log"
)

func sqlBlock() error {

	// Create table: block
	table := "block"

	log.Println("- " + table)
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS ` + table + ` (
				"id"          SERIAL,
				"list"        INT,
				"state"       VARCHAR(10),
				"indexId"     INT,
				"sizeId"      INT,
				"replicas"    INT,
				"when"        TIMESTAMPTZ,
				"who"         VARCHAR(255),
				"key"         UUID,
				PRIMARY KEY(id)
				);`)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
