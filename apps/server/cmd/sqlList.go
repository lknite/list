package main

import (
	"log"
)

func sqlList() error {

	// Create table: list
	table := "list"

	log.Println("- " + table)
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS ` + table + ` (
				"id"          SERIAL,
				"when"        TIMESTAMPTZ,
				"who"         VARCHAR(255),
				"name"        VARCHAR(255),
				"task"        VARCHAR(255),
				"action"      VARCHAR(255),
				"state"       VARCHAR(10),
				"access"      VARCHAR(255),
				"totalId"     INT,
				"sizeId"      INT,
				PRIMARY KEY(id)
				);`)
	if err != nil {
		return err
	}

	return nil
}
