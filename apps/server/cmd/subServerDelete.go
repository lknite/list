package main

import (
	"context"
	"encoding/json"
	"log"

	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/model"
)

func serverDelete(m *nats.Msg) {
	// log
	//log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	log.Println("[delete /key] name: ", request.Values.Get("name"))

	// get lock on key
	var key model.KeyRef
	err = tx.QueryRowContext(ctx, "SELECT * FROM key WHERE name=$1 FOR UPDATE;", request.Values.Get("name")).Scan(
		&key.Id, &key.Who, &key.Hash, &key.Exp, &key.Name, &key.Claims)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("key found: %d", key.Id)

	// delete key
	_, err = tx.ExecContext(ctx, "DELETE FROM key WHERE name=$1;", request.Values.Get("name"))

	// Commit the transaction.
	log.Printf("commiting")
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return
	}

	// respond to message with result
	m.Respond([]byte("ok"))
}
