package main

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"log"
	rnd "math/rand"
	"net/http"
	"strings"
	"time"

	_ "github.com/lib/pq"

	"github.com/golang-jwt/jwt/v5"
)

type EnsureAuth struct {
	handler http.Handler
}

type contextKey int

const authenticatedUserKey contextKey = 0

// (middleware) Authenticate via Bearer Token and pass along claims to httpHandle
func (ea *EnsureAuth) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	claims, err := GetAuthenticatedUser(r)
	if err != nil {
		/*
			// add this section if '/login' & '/callback' are implemented as they need to be exceptions to token requirement

			// login url is an exception to the normal authentication
			if strings.HasPrefix(r.RequestURI, "/login") || strings.HasPrefix(r.RequestURI, "/callback") {
				log.Println("pass-through: " + r.RequestURI)
				ea.handler.ServeHTTP(w, r)
				return
			}
		*/

		http.Error(w, "authentication required", http.StatusUnauthorized)
		return
	}

	//create a new request context containing the authenticated user
	ctxWithUser := context.WithValue(r.Context(), authenticatedUserKey, claims)
	//create a new request using that new context
	rWithUser := r.WithContext(ctxWithUser)
	//call the real handler, passing the new request
	ea.handler.ServeHTTP(w, rWithUser)
}

func NewEnsureAuth(handlerToWrap http.Handler) *EnsureAuth {
	return &EnsureAuth{handlerToWrap}
}

// (middleware) Authenticate via Bearer Token and pass along claims to httpHandle
func GetAuthenticatedUser(r *http.Request) (jwt.MapClaims, error) {
	//validate the session token in the request,
	//fetch the session state from the session store,
	//and return the authenticated user
	//or an error if the user is not authenticated

	// Bearer token is detected if it starts with 'Bearer '
	tokenString, found := strings.CutPrefix(r.Header.Get("Authorization"), "Bearer ")
	if found {

		// parse the JWT
		// jwks.Keyfunc will verify the token, it was initialized before with available public keys provided by oidc server
		token, err := jwt.Parse(tokenString, jwks.Keyfunc)
		if err != nil {
			log.Fatalf("Failed to parse the JWT, or verification failed.\nError: %s", err)
		}

		//
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			return claims, nil
		}

		return nil, errors.New("authentication failed (bearer token)")

	} else if r.Header.Get("API-KEY") != "" {
		key := r.Header.Get("API-KEY")

		// get context to use with transaction
		ctx := r.Context()

		// apiKey consists of <email>:<password>, parse off email & password
		parts := strings.SplitN(key, ":", 2)
		if len(parts) != 2 {
			return nil, errors.New("authentication failed, malformed api-key")
		}
		email := parts[0]
		password := parts[1]

		// get hash encoding of apiKey
		hash := sha256.Sum256([]byte(key))
		hashb64 := base64.StdEncoding.EncodeToString([]byte(hash[:]))
		log.Printf("hash: %v\n", hash)
		log.Printf("hashstr: %v\n", hashb64)

		var j string
		var claimsb64 string
		//log.Printf("SELECT \"claims\" FROM key WHERE \"who\"='%v' AND \"hash\"='%v'", email, hashb64)
		err := db.QueryRowContext(ctx, `SELECT "claims" FROM key WHERE "who"=$1 AND "hash"=$2`, email, hashb64).Scan(&claimsb64)
		if err == nil {
			log.Printf("apiKey found, obtaining claims")

			// for aes the 'secret' must be 32 bytes (a guid is 32 if you take out the '-')
			secretKey := strings.Join(strings.Split(password, "-"), "")

			//
			cipherText, err := base64.StdEncoding.DecodeString(claimsb64)
			if err != nil {
				return nil, errors.New("authentication failed (api-key)")
			}

			// decrypt claims from storage (to json)
			j = decrypt(secretKey, string(cipherText))

			// convert j into claims
			var claims jwt.MapClaims
			json.Unmarshal([]byte(j), &claims)

			return claims, nil
		}
		log.Printf("error: %v\n", err.Error())
		log.Printf("apiKey not found")

		// insert random delay, this prevents timing-based brute-force apiKey password attacks
		r := rnd.Intn(200)
		time.Sleep(time.Duration(r) * time.Microsecond)

		return nil, errors.New("authentication failed (api-key)")

	}

	return nil, errors.New("authentication required")
}

// used with websocket, where token must be sent with first message, rather than via header
func GetAuthenticatedUserByToken(authToken string) (jwt.MapClaims, error) {

	// Bearer token is detected if it starts with 'Bearer '
	tokenString, found := strings.CutPrefix(authToken, "Bearer ")
	if found {

		// parse the JWT
		// jwks.Keyfunc will verify the token, it was initialized before with available public keys provided by oidc server
		token, err := jwt.Parse(tokenString, jwks.Keyfunc)
		if err != nil {
			log.Fatalf("Failed to parse the JWT, or verification failed.\nError: %s", err)
		}

		// if token is valid obtain and return claims
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			return claims, nil
		}
	} else {
		// if not a bearer token, then maybe an apikey
		key := tokenString

		// apiKey consists of <email>:<password>, parse off email & password
		parts := strings.SplitN(key, ":", 2)
		if len(parts) != 2 {
			return nil, errors.New("authentication failed, malformed api-key")
		}
		email := parts[0]
		password := parts[1]

		// get hash encoding of apiKey
		hash := sha256.Sum256([]byte(key))
		hashb64 := base64.StdEncoding.EncodeToString([]byte(hash[:]))
		log.Printf("hash: %v\n", hash)
		log.Printf("hashstr: %v\n", hashb64)

		var j string
		var claimsb64 string
		//log.Printf("SELECT \"claims\" FROM key WHERE \"who\"='%v' AND \"hash\"='%v'", email, hashb64)
		err := db.QueryRow(`SELECT "claims" FROM key WHERE "who"=$1 AND "hash"=$2`, email, hashb64).Scan(&claimsb64)
		if err == nil {
			log.Printf("apiKey found, obtaining claims")

			// for aes the 'secret' must be 32 bytes (a guid is 32 if you take out the '-')
			secretKey := strings.Join(strings.Split(password, "-"), "")

			//
			cipherText, err := base64.StdEncoding.DecodeString(claimsb64)
			if err != nil {
				return nil, errors.New("authentication failed (api-key)")
			}

			// decrypt claims from storage (to json)
			j = decrypt(secretKey, string(cipherText))

			// convert j into claims
			var claims jwt.MapClaims
			json.Unmarshal([]byte(j), &claims)

			return claims, nil
		}
		log.Printf("error: %v\n", err.Error())
		log.Printf("apiKey not found")

		// insert random delay, this prevents timing-based brute-force apiKey password attacks
		r := rnd.Intn(200)
		time.Sleep(time.Duration(r) * time.Microsecond)

		return nil, errors.New("authentication failed (api-key)")
	}

	return nil, errors.New("authentication failed, websocket (bearer token)")
}

// use to encrypt claims for storage
// reference: https://dev.to/breda/secret-key-encryption-with-go-using-aes-316d
func encrypt(secretKey string, plaintext string) string {
	aes, err := aes.NewCipher([]byte(secretKey))
	if err != nil {
		panic(err)
	}

	gcm, err := cipher.NewGCM(aes)
	if err != nil {
		panic(err)
	}

	// We need a 12-byte nonce for GCM (modifiable if you use cipher.NewGCMWithNonceSize())
	// A nonce should always be randomly generated for every encryption.
	nonce := make([]byte, gcm.NonceSize())
	_, err = rand.Read(nonce)
	if err != nil {
		panic(err)
	}

	// ciphertext here is actually nonce+ciphertext
	// So that when we decrypt, just knowing the nonce size
	// is enough to separate it from the ciphertext.
	ciphertext := gcm.Seal(nonce, nonce, []byte(plaintext), nil)

	return string(ciphertext)
}

// use to decrypt claims from storage
// reference: https://dev.to/breda/secret-key-encryption-with-go-using-aes-316d
func decrypt(secretKey string, ciphertext string) string {
	aes, err := aes.NewCipher([]byte(secretKey))
	if err != nil {
		panic(err)
	}

	gcm, err := cipher.NewGCM(aes)
	if err != nil {
		panic(err)
	}

	// Since we know the ciphertext is actually nonce+ciphertext
	// And len(nonce) == NonceSize(). We can separate the two.
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]

	plaintext, err := gcm.Open(nil, []byte(nonce), []byte(ciphertext), nil)
	if err != nil {
		panic(err)
	}

	return string(plaintext)
}
