package main

import (
	"context"
	"encoding/json"
	"log"

	"github.com/nats-io/nats.go"
	"gitlab.com/lknite/list/internal/model"
)

func serverGet(m *nats.Msg) {
	// log
	//log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	// mapped json
	var keys []model.KeyRef

	// get lists (loop through results and build up bignums)
	rows, err := tx.QueryContext(ctx, "SELECT * FROM key WHERE who=$1;", request.Who)
	if err != nil {
		log.Println(err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var key model.KeyRef
		err = rows.Scan(&key.Id, &key.Who, &key.Hash, &key.Exp, &key.Name, &key.Claims)
		keys = append(keys, key)
		if err != nil {
			log.Println(err)
			return
		}
	}

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Fatal(err)
	}

	// respond to message with result
	j, _ := json.Marshal(keys)
	m.Respond(j)
}
