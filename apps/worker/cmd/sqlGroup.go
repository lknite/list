package main

import (
	"log"
)

func sqlGroup() error {

	// Create table: group
	table := "group"

	log.Println("- " + table)
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS "` + table + `" (
				"id"          SERIAL,
				"list"        INT,
				"kid"         VARCHAR(25),
				"group"       VARCHAR(256),
				"ro"          BOOLEAN,
				"rw"          BOOLEAN,
				PRIMARY KEY(id)
				);`)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}
