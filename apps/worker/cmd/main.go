// trigger: Sat Jun 29 06:45:46 PM MDT 2024
package main

import (
	"context"
	"database/sql"
	"log"
	"sync"

	"github.com/MicahParks/keyfunc/v3"
	_ "github.com/lib/pq"
	"github.com/nats-io/nats.go"
)

var (
	// wait group to ensure a clean exit
	wg sync.WaitGroup

	// global shared sql db connection
	db *sql.DB

	// global shared nats server connection
	nc *nats.Conn

	//
	jwks keyfunc.Keyfunc
)

func main() {
	/**
	 * initialize
	 */

	// note: if an outside resource is needed, these will wait until they are available
	initLog()
	initDb()
	initDbTables()
	initNats()
	initJwks()

	// drain the nats server connection upon exit
	defer nc.Drain()
	// close the database connection upon exit
	defer db.Close()

	/**
	 * start handlers with a context which can be cancelled
	 */

	// context which can be cancelled, makes for a clean exit across the whole application
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// http handlers
	startApiHandlers(ctx)
	// nats subscribers
	startSubscribers(ctx)
	// kubernetes controllers
	//startControllers(ctx)

	/**
	 * wait for all handlers to exit before exiting
	 */
	wg.Wait()

	// clean exit
	log.Println("all done, exiting")
}
