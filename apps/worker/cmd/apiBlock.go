package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/nats-io/nats.go"
	"gitlab.com/lknite/list/internal/model"

	"github.com/golang-jwt/jwt/v5"
)

func BlockInit(mux *http.ServeMux) {

	BlockInitHandles(mux)
}

func BlockInitHandles(mux *http.ServeMux) {

	// implementation is here, rather than in controller because we would
	// otherwise end up sending a very large amount of data twice
	mux.HandleFunc("/block/big", func(w http.ResponseWriter, r *http.Request) {
		// get the authenticated user claims from the request context
		claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
		log.Println("claims pass-through: " + fmt.Sprintf("%v", claims["email"]))

		// -- handle request

		switch r.Method {
		case http.MethodGet:
			// Get a record
			log.Println("[get] /block/big")

			// access parameters passed in via the url
			values := r.URL.Query()
			if !values.Has("id") {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte("block id must be specified as url parameter"))
				return
			}
			if !values.Has("key") {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte("block key must be specified as url parameter"))
				return
			}
			if !values.Has("type") {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte("block type must be specified as url parameter, either type=index or type=size"))
				return
			}

			// get context to use with transaction
			ctx := nats.Context(context.Background())

			// get bignum meta data
			var bigId int
			if values.Get("type") == "index" {
				id, err := strconv.Atoi(values.Get("id"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("error: converting id"))
					return
				}
				key, err := uuid.Parse(values.Get("key"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("error: converting key"))
					return
				}
				err = db.QueryRowContext(ctx, `SELECT "indexId" FROM block WHERE "id"=$1 AND "key"=$2`, id, key).Scan(&bigId)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("invalid id/key/type combination"))
					return
				}
			} else {
				id, err := strconv.Atoi(values.Get("id"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("error: converting id"))
					return
				}
				key, err := uuid.Parse(values.Get("key"))
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("error: converting key"))
					return
				}
				err = db.QueryRowContext(ctx, `SELECT "sizeId" FROM block WHERE "id"=$1 AND "key"=$2`, id, key).Scan(&bigId)
				if err != nil {
					log.Println(err)
					w.WriteHeader(http.StatusNotFound)
					w.Write([]byte("invalid id/key/type combination"))
					return
				}
			}

			// get big record
			var big model.Big
			err := db.QueryRowContext(ctx, `SELECT "n", "m", "r" FROM big WHERE "id"=$1`, bigId).Scan(&big.N, &big.M, &big.R)
			if err != nil {
				log.Println(err)
				return
			}

			// get big parts
			rows, err := db.QueryContext(ctx, `SELECT "index", "part", "hash" FROM big_part WHERE "bigId"=$1;`, bigId)
			if err != nil {
				log.Println(err)
				return
			}
			defer rows.Close()
			for rows.Next() {
				var p model.BigPart
				err = rows.Scan(&p.Index, &p.Part, &p.Hash)
				if err != nil {
					log.Println(err)
					return
				}

				big.Parts = append(big.Parts, p)

				// send a part at a time rather than all at once
				w.Write([]byte(p.Part))
			}
		}
	})

	mux.HandleFunc("/block", func(w http.ResponseWriter, r *http.Request) {
		// get the authenticated user claims from the request context
		claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
		log.Println("claims pass-through: " + fmt.Sprintf("%v", claims["email"]))

		// -- handle request

		switch r.Method {
		case http.MethodGet:
			/**
			 * get available block, or generate & get new block
			 */
			log.Println("[get] /block")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("block.get", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			w.Write(m.Data)

		case http.MethodPost:
			// Create a record
			// (use GET instead of POST to checkout a block)

		case http.MethodPut:
			/**
			 * Update an existing record.
			 * (mark block as complete)
			 */
			log.Println("[put] /block")

			// Prep request
			var request model.Request
			request.When = time.Now()
			request.Who = fmt.Sprintf("%v", claims["email"])
			request.Values = r.URL.Query()
			request.Data, _ = io.ReadAll(r.Body)
			request.Claims = claims

			// (nats) Send event to subscribers, and wait for a reply
			j, _ := json.Marshal(request)
			m, err := nc.Request("block.put", j, time.Second*20)
			if err != nil {
				log.Fatal(err)
			}

			w.Write(m.Data)

		case http.MethodDelete:
			// Remove the record.

		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		}
	})
}
