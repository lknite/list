package main

import (
	"context"
	"log"
)

func startSubscribers(ctx context.Context) {
	/**
	 * controller: message queue handlers
	 */

	log.Println("queue subscribers:")

	// controller-lists

	log.Println("- ", "list.get")
	nc.QueueSubscribe("list.get", "list.get", listGet)

	log.Println("- ", "list.post")
	nc.QueueSubscribe("list.post", "list.post", listPost)

	log.Println("- ", "list.put")
	nc.QueueSubscribe("list.put", "list.put", listPut)

	log.Println("- ", "list.delete")
	nc.QueueSubscribe("list.delete", "list.delete", listDelete)

	// controller-blocks

	log.Println("- ", "block.get")
	nc.QueueSubscribe("block.get", "block.get", blockGet)

	log.Println("- ", "block.put")
	nc.QueueSubscribe("block.put", "block.put", blockPut)

	// controller-key

	log.Println("- ", "key.get")
	nc.QueueSubscribe("key.get", "key.get", keyGet)

	log.Println("- ", "key.post")
	nc.QueueSubscribe("key.post", "key.post", keyPost)

	log.Println("- ", "key.delete")
	nc.QueueSubscribe("key.delete", "key.delete", keyDelete)

	// controller-server

	log.Println("- ", "server.get")
	nc.QueueSubscribe("server.get", "server.get", serverGet)

	log.Println("- ", "server.post")
	nc.QueueSubscribe("server.post", "server.post", serverPost)

	log.Println("- ", "server.put")
	nc.QueueSubscribe("server.put", "server.put", serverPut)

	log.Println("- ", "server.delete")
	nc.QueueSubscribe("server.delete", "server.delete", serverDelete)
}
