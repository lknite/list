package main

import (
	"context"
	"encoding/json"
	"log"

	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/model"
)

func listPut(m *nats.Msg) {
	// log
	log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	// set state
	var list model.ListRef
	switch request.Values.Get("state") {
	case "suspend":
		// get list
		log.Println("[put /list] select list 'suspend'")
		err = tx.QueryRowContext(ctx, "SELECT * FROM list WHERE name=$1 AND state=$2 FOR UPDATE;", request.Values.Get("name"), "active").Scan(
			&list.Id, &list.When, &list.Who, &list.Name, &list.Task, &list.Action, &list.State, &list.Access, &list.TotalId, &list.SizeId)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("list found: %d", list.Id)

		//
		log.Printf("suspending list: %d", list.Id)
		_, err := tx.ExecContext(ctx, `UPDATE list set "state"=$1 WHERE "id"=$2;`, "suspended", list.Id)
		if err != nil {
			log.Println(err)
			return
		}

		break
	case "resume":
		// get list
		log.Println("[put /list] select list 'resume'")
		err = tx.QueryRowContext(ctx, "SELECT * FROM list WHERE name=$1 AND state=$2 FOR UPDATE;", request.Values.Get("name"), "suspended").Scan(
			&list.Id, &list.When, &list.Who, &list.Name, &list.Task, &list.Action, &list.State, &list.Access, &list.TotalId, &list.SizeId)
		if err != nil {
			log.Println(err)
			return
		}
		log.Printf("list found: %d", list.Id)

		//
		log.Printf("resuming list: %d", list.Id)
		_, err := tx.ExecContext(ctx, `UPDATE list set "state"=$1 WHERE "id"=$2;`, "active", list.Id)
		if err != nil {
			log.Println(err)
			return
		}

		break
	default:
		log.Printf("unknown state requested: %v", request.Values.Get("state"))
		m.Respond([]byte("error"))
		break
	}

	// Commit the transaction.
	log.Printf("commiting")
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return
	}

	// send to listeners
	log.Printf("send to listeners")
	switch request.Values.Get("state") {
	case "suspend":
		nc.Publish("list.suspend", []byte("{ \"list.suspend\": \""+list.Name+"\" }"))
		break
	case "resume":
		nc.Publish("list.resume", []byte("{ \"list.resume\": \""+list.Name+"\" }"))
		break
	}

	// respond to message with result
	//j, _ := json.Marshal("ok")
	//m.Respond(j)
	m.Respond([]byte("ok"))
}
