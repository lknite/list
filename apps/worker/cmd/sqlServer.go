package main

import (
	"log"
)

func sqlServer() error {

	// Create table: server
	table := "server"

	log.Println("- " + table)
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS ` + table + ` (
				"id"          SERIAL,
				"name"        VARCHAR(255),
				"value"       VARCHAR(255),
				PRIMARY KEY(id)
				);`)
	if err != nil {
		return err
	}

	return nil
}
