package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"math/big"
	"strconv"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go"
	"gitlab.com/lknite/list/internal/dbbig"
	"gitlab.com/lknite/list/internal/model"
)

func blockPut(m *nats.Msg) {
	// log
	log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	// get list
	log.Println("[put /block] select list")
	var list model.ListRef
	err = tx.QueryRowContext(ctx, "SELECT * FROM list WHERE name=$1 AND state=$2;", request.Values.Get("name"), "active").Scan(
		&list.Id, &list.When, &list.Who, &list.Name, &list.Task, &list.Action, &list.State, &list.Access, &list.TotalId, &list.SizeId)
	if err != nil {
		log.Println(err)
		return
	}

	// debug
	out, _ := json.MarshalIndent(list, "", "  ")
	log.Printf("%v\n", string(out))

	// get lock on 'root' block
	log.Println("[put /block] get lock on 'root' block")
	var block model.BlockRef
	err = tx.QueryRowContext(ctx, "SELECT * FROM block WHERE list=$1 AND state=$2 FOR UPDATE;", list.Id, "root").Scan(
		&block.Id, &block.List, &block.State, &block.IndexId, &block.SizeId, &block.Replicas, &block.When, &block.Who, &block.Key)
	if err != nil {
		// this root block was created when the list was created, so this should always succeed
		// (will wait if someone else has a lock, without erroring)
		log.Println(err)
		return
	}

	// debug
	out, _ = json.MarshalIndent(block, "", "  ")
	log.Printf("%v\n", string(out))

	// get passed in values
	id, err := strconv.Atoi(request.Values.Get("id"))
	key, err := uuid.Parse(request.Values.Get("key"))

	// set block as complete
	rows, err := tx.ExecContext(ctx, `UPDATE block SET "state"=$1 WHERE "list"=$2 AND "id"=$3 AND "key"=$4 AND "state"=$5;`,
		"complete", block.List, id, key, "active")
	if err != nil {
		log.Println(err)
		return
	}

	// get number of rows affected, should 1 on success, 0 otherwise
	rowsAffected, err := rows.RowsAffected()

	// if push was successful then merge blocks, if possible
	if rowsAffected > 0 {
		isComplete := Merge(ctx, tx, list)
		if isComplete {
			log.Println("list is complete")

			_, err := tx.ExecContext(ctx, `UPDATE list SET "state"=$1 WHERE "id"=$2;`,
				"complete", list.Id)
			if err != nil {
				log.Println(err)
				return
			}

			// send to listeners
			nc.Publish("list.complete", []byte("{ \"list.complete\": "+strconv.Itoa(list.Id)+" }"))
		}
	}

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Fatal(err)
	}

	// respond to message with result
	j, _ := json.Marshal(rowsAffected)
	m.Respond(j)
}

func Merge(ctx nats.ContextOpt, tx *sql.Tx, list model.ListRef) bool {
	log.Println("MERGE")

	rows, err := tx.QueryContext(ctx, "SELECT * FROM block WHERE list=$1 AND state=$2 ORDER BY id;", list.Id, "complete")
	if err != nil {
		log.Println(err)
		return false
	}
	defer rows.Close()

	var blocks []model.BlockRef
	for rows.Next() {
		var block model.BlockRef
		err = rows.Scan(&block.Id, &block.List, &block.State, &block.IndexId, &block.SizeId, &block.Replicas, &block.When, &block.Who, &block.Key)
		blocks = append(blocks, block)
		if err != nil {
			log.Println(err)
			return false
		}
	}

	// display what we found
	isFirst := true
	var prev model.BlockRef
	for i, next := range blocks {
		// in first loop we set 'prev' and advanced to next block
		if isFirst {
			prev = next
			isFirst = false

			continue
		}

		// we have a 'prev' and 'next', check for merge conditions
		log.Println("")

		// debug
		log.Println("prev: ", i)
		data, err := json.MarshalIndent(prev, "", "  ")
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string(data))

		// debug
		log.Println("next: ", i)
		data, err = json.MarshalIndent(next, "", "  ")
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(string(data))

		// load up bignums for testing
		dbPrevIndex, err := dbbig.Load(db, prev.IndexId)
		dbPrevSize, err := dbbig.Load(db, prev.SizeId)
		dbNextIndex, err := dbbig.Load(db, next.IndexId)
		dbNextSize, err := dbbig.Load(db, next.SizeId)

		// are 'prev' and 'next' sibling blocks?
		test := big.NewInt(0)
		test.Add(dbPrevIndex.Value, dbPrevSize.Value)
		if test.Cmp(dbNextIndex.Value) == 0 {
			// merge next into prev
			dbPrevSize.Value.Add(dbPrevSize.Value, dbNextSize.Value)
			dbPrevSize.Save()

			// delete big 'index' associated with block
			dbNextIndex.Delete()

			// delete big 'size' associated with block
			dbNextSize.Delete()

			// delete 'next' block
			_, err := tx.ExecContext(ctx, `DELETE FROM block WHERE "id"=$1;`,
				next.Id)
			if err != nil {
				log.Println(err)
				return false
			}

			// 'prev' remains the same, continue looking for more blocks to merge
			continue
		}

		// remember this block as 'prev' for next loop
		prev = next
	}

	// is the list complete?
	dbPrevSize, err := dbbig.Load(db, prev.SizeId)
	dbListTotal, err := dbbig.Load(db, list.TotalId)
	if dbPrevSize.Value.Cmp(dbListTotal.Value) == 0 {
		return true
	}

	return false
}
