package main

import (
	"log"
	"os"
)

func sqlDb() error {

	// Create database as specified via environment variable
	database, found := os.LookupEnv("DB")
	if !found {
		log.Fatalf("environment variable missing: %v\n", "DB")
	}

	log.Println("- create database: " + database)
	_, err := db.Exec(`CREATE DATABASE ` + database + `;`)
	if err != nil {
		// ignore create database error, we can't do "if not exists" like a table
		return err
	}

	return nil
}
