package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	_ "github.com/lib/pq"
	"github.com/nats-io/nats.go"
)

func WebsocketInit() {

	WebsocketInitHandles()
}

func WebsocketInitHandles() {
	// start up health checks on their own thread
	wg.Add(1)
	go func() {
		defer wg.Done()

		http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
			//get the authenticated user from the request context
			//claims := r.Context().Value(authenticatedUserKey).(jwt.MapClaims)
			//fmt.Println("/ws", claims["name"], claims["preferred_username"], claims["email"])

			// upgrade connection to a websocket
			conn, _, _, err := ws.UpgradeHTTP(r, w)
			if err != nil {
				// handle error
				log.Printf("UpgradeHTTP: %v\n", err)
				return
			}

			// first client message is expected to be authentication
			msg, op, err := wsutil.ReadClientData(conn)
			//msg, op, err := wsutil.ReadClientData(conn)
			if err != nil {
				// handle error
				log.Printf("ReadClientData error: %v\n", err)
				conn.Close()
				return
			}
			//log.Println("msg: %v", msg)
			log.Printf("op: %v\n", op)

			// using provided token acquire claims
			claims, err := GetAuthenticatedUserByToken(string(msg))
			if err != nil {
				// handle error
				log.Printf("GetAuthenticatedUser error: %v\n", err)
				msg = []byte("{\"auth\": \"failed\"}")
				err = wsutil.WriteServerMessage(conn, op, msg)
				conn.Close()
				return
			}

			// websocket has been authenticated, claims are now available
			log.Println("auth successful:")
			log.Println("- name: ", claims["name"])
			log.Println("- email: ", claims["email"])
			log.Println("- preferred_username: ", claims["preferred_username"])
			email := fmt.Sprintf("%v", claims["email"])

			msg = []byte("{\"auth\": \"successful\"}")
			err = wsutil.WriteServerMessage(conn, op, msg)
			if err != nil {
				// handle error
				log.Println("WriteServerMessage error: ", err)
				conn.Close()
				return
			}

			// (nats) watch message queue and send to websocket
			go func() {
				// Drain connection (Preferred for responders)
				defer nc.Drain()

				// Simple Async Subscriber
				log.Println("subscribe to:", "email."+email)
				nc.Subscribe("email."+email, func(m *nats.Msg) {
					//
					fmt.Printf("Received a message: %s\n", string(m.Data))

					// forward message to websocket
					err := wsutil.WriteServerMessage(conn, op, m.Data)
					if err != nil {
						log.Println("NATS error: ", err)
						return
					}
					_ = msg
					_ = op
				})

				//
				log.Println("subscribe to:", "list.create")
				nc.Subscribe("list.create", func(m *nats.Msg) {
					//
					fmt.Printf("Received a message: %s\n", string(m.Data))

					// forward message to websocket
					err := wsutil.WriteServerMessage(conn, op, m.Data)
					if err != nil {
						log.Println("NATS error: ", err)
						return
					}
					_ = msg
					_ = op
				})

				//
				log.Println("subscribe to:", "list.suspend")
				nc.Subscribe("list.suspend", func(m *nats.Msg) {
					//
					fmt.Printf("Received a message: %s\n", string(m.Data))

					// forward message to websocket
					err := wsutil.WriteServerMessage(conn, op, m.Data)
					if err != nil {
						log.Println("NATS error: ", err)
						return
					}
					_ = msg
					_ = op
				})

				//
				log.Println("subscribe to:", "list.resume")
				nc.Subscribe("list.resume", func(m *nats.Msg) {
					//
					fmt.Printf("Received a message: %s\n", string(m.Data))

					// forward message to websocket
					err := wsutil.WriteServerMessage(conn, op, m.Data)
					if err != nil {
						log.Println("NATS error: ", err)
						return
					}
					_ = msg
					_ = op
				})

				//
				log.Println("subscribe to:", "list.delete")
				nc.Subscribe("list.delete", func(m *nats.Msg) {
					//
					fmt.Printf("Received a message: %s\n", string(m.Data))

					// forward message to websocket
					err := wsutil.WriteServerMessage(conn, op, m.Data)
					if err != nil {
						log.Println("NATS error: ", err)
						return
					}
					_ = msg
					_ = op
				})

				// Cleanup websocket if connection drops
				defer conn.Close()

				// use this heartbeat to keep the NATS connection alive above, if websocket exits then NATS exits also
				for {
					// 10 second interval
					time.Sleep(10 * 1000 * time.Millisecond)

					// send ping to client
					err = wsutil.WriteServerMessage(conn, ws.OpPing, []byte("")) //"this is a heartbeat"))
					if err != nil {
						log.Println("WriteServerMessage error: ", err)
						return
					}
				}
			}()
		})

		// create a 'server' so we can later use 'shutdown'
		srv := &http.Server{
			Addr: ":8082",
		}

		// handle SIGINT / SIGTERM
		go func() {
			sigint := make(chan os.Signal)
			signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
			<-sigint

			log.Println("- shutdownWebsocketHandler")

			// cancel routine during shutdown, if needed
			sigctx, sigcancel := context.WithTimeout(context.Background(), 20*time.Second)
			defer func() {
				// extra handling here
				sigcancel()
			}()

			// stop listening, finish calls in progress, then exit
			if err := srv.Shutdown(sigctx); err != nil {
				log.Fatalf("- shutdownWebsocketHandler: %+v", err)
			}
			log.Print("- shutdownWebsocketHandler exited")
		}()

		// health checks run on a different, non-authenticated port
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
}
