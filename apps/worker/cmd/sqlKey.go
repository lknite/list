package main

import (
	"log"
)

func sqlAuth() error {

	// Create table: auth
	table := "key"

	log.Println("- " + table)
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS ` + table + ` (
				"id"          SERIAL,
				"who"         VARCHAR(255),
				"hash"        VARCHAR(255),
				"exp"         INT,
				"name"        VARCHAR(255),
				"claims"      TEXT,
				PRIMARY KEY(id)
				);`)
	if err != nil {
		return err
	}

	return nil
}
