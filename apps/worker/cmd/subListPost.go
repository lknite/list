package main

import (
	"context"
	"encoding/json"
	"log"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/dbbig"
	"gitlab.com/lknite/list/internal/model"
)

func listPost(m *nats.Msg) {
	// log
	log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	// mapped json
	type Data struct {
		Name   string `json:"name"`
		Task   string `json:"task"`
		Action string `json:"action"`
		Total  string `json:"total"`
		Size   string `json:"size"`
	}
	var data Data

	// parse data into struct
	json.Unmarshal(request.Data, &data)

	// create new bignum 'total' for list
	dbListTotal, err := dbbig.New(db, data.Total)
	if err != nil {
		log.Println(err)
		return
	}
	// save to database
	dbListTotal.Save()

	// create new bignum 'size' for list
	dbListSize, err := dbbig.New(db, data.Size)
	if err != nil {
		log.Println(err)
		return
	}
	// save to database
	dbListSize.Save()

	// create a new list record in database
	var listId int
	err = tx.QueryRowContext(ctx, `INSERT INTO list ("when", "who", "name", "task", "action", "state", "access", "totalId", "sizeId") VALUES (CURRENT_TIMESTAMP, $1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;`,
		request.Who, data.Name, data.Task, data.Action, "active", "public", dbListTotal.GetId(), dbListSize.GetId()).Scan(&listId)
	if err != nil {
		log.Println(err)
		return
	}
	// Return id of new list
	log.Printf("request: %v, [POST] /list, listId: %d\n", "?", listId)

	// create initial root block with values used to generate blocks
	key, err := uuid.NewUUID()
	if err != nil {
		log.Println(err)
		return
	}

	// create new bignum 'index' for 'root' block, always starts at '0'
	dbRootIndex, err := dbbig.New(db, "0")
	if err != nil {
		log.Println(err)
		return
	}
	// save to database
	dbRootIndex.Save()

	// dbListSize.GetId() is used for the size in the 'root' block, no need to create a new bignum

	var rowId int
	err = tx.QueryRowContext(ctx, `INSERT INTO block ("list", "state", "indexId", "sizeId", "replicas", "when", "who", "key") VALUES ($1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $6, $7) RETURNING id;`,
		listId, "root", dbRootIndex.GetId(), dbListSize.GetId(), 1, request.Who, key.String()).Scan(&rowId)
	if err != nil {
		log.Println(err)
		return
	}

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return
	}

	// send to listeners
	nc.Publish("list.create", []byte("{ \"list.create\": \""+data.Name+"\" }"))

	// respond to message with result
	j, _ := json.Marshal("ok")
	m.Respond(j)
}
