package main

import (
	"log"
)

func sqlBig() error {

	// Create table: big
	table := "big"

	log.Println("- " + table)
	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS ` + table + ` (
				"id"          SERIAL,
				"n"           INT,
				"m"           INT,
				"r"           INT,
				PRIMARY KEY(id)
				);`)
	if err != nil {
		return err
	}

	// Create table: big_part
	table = "big_part"

	log.Println("- " + table)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS ` + table + ` (
				"id"          SERIAL,
				"bigId"       INT,
				"index"       INT,
				"part"        TEXT,
				"hash"        VARCHAR(255),
				PRIMARY KEY(id)
				);`)
	if err != nil {
		return err
	}

	return nil
}
