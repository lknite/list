package main

import (
	"context"
	"encoding/json"
	"log"
	"math/big"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/dbbig"
	"gitlab.com/lknite/list/internal/model"
)

func blockGet(m *nats.Msg) {
	// log
	log.Printf("%s\n", string(m.Data))

	// parse the message
	var request model.Request
	json.Unmarshal(m.Data, &request)

	// debug
	log.Printf("email: %v", request.Who)

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer tx.Rollback()

	/**
	 * BEGIN
	 */

	// get list
	log.Println("[get /block] select list")
	var list model.ListRef
	err = tx.QueryRowContext(ctx, "SELECT * FROM list WHERE name=$1 AND state=$2;", request.Values.Get("name"), "active").Scan(
		&list.Id, &list.When, &list.Who, &list.Name, &list.Task, &list.Action, &list.State, &list.Access, &list.TotalId, &list.SizeId)
	if err != nil {
		log.Println(err)
		return
	}

	// debug
	out, _ := json.MarshalIndent(list, "", "  ")
	log.Printf("%v\n", string(out))

	// get lock on 'root' block
	log.Println("[get /block] get lock on 'root' block")
	var root model.BlockRef
	err = tx.QueryRowContext(ctx, `SELECT * FROM block WHERE "list"=$1 AND "state"=$2 FOR UPDATE;`, list.Id, "root").Scan(
		&root.Id, &root.List, &root.State, &root.IndexId, &root.SizeId, &root.Replicas, &root.When, &root.Who, &root.Key)
	if err != nil {
		// this root block was created when the list was created, so this should always succeed
		// (will wait if someone else has a lock, without erroring)
		log.Println(err)
		return
	}

	// debug
	out, _ = json.MarshalIndent(root, "", "  ")
	log.Printf("%v\n", string(out))

	// look for blocks to redeploy
	log.Println("[get /block] look for blocks to redeploy")

	var b model.BlockRef
	err = tx.QueryRowContext(ctx, `SELECT * FROM block WHERE "list"=$1 AND "state"=$2 AND CURRENT_TIMESTAMP>("when"+ INTERVAL '30 seconds');`, list.Id, "active").Scan(
		&b.Id, &b.List, &b.State, &b.IndexId, &b.SizeId, &b.Replicas, &b.When, &b.Who, &b.Key)
	if err != nil {
		log.Println(err)
	} else {
		log.Println("[get /block] found a block to redeploy: ", b.Id)

		/*
			// create new key using a random uuid
			var rowId int
			key, err := uuid.NewUUID()
			if err != nil {
				log.Println(err)
				return
			}
		*/

		// update block with new key & new timestamp
		_, err := tx.ExecContext(ctx, `UPDATE block SET "key"=$1,"when"=CURRENT_TIMESTAMP WHERE "id"=$2;`,
			b.Key, b.Id)
		if err != nil {
			log.Println(err)
			return
		}

		// Commit the transaction.
		if err = tx.Commit(); err != nil {
			log.Fatal(err)
		}

		// respond to message with result
		j, _ := json.Marshal(b)
		m.Respond(j)

		return
	}

	// create new block
	log.Println("[get /block] create new block")

	// create new key using a random uuid
	var rowId int
	key, err := uuid.NewUUID()
	if err != nil {
		log.Println(err)
		return
	}
	// load up the 'root' block bignum index
	dbRootIndex, err := dbbig.Load(db, root.IndexId)
	if err != nil {
		log.Println(err)
		return
	}

	// load up the 'total' list bignum index
	dbListTotal, err := dbbig.Load(db, list.TotalId)
	if err != nil {
		log.Println(err)
		return
	}
	// is index >= total
	if dbRootIndex.Value.Cmp(dbListTotal.Value) != -1 {
		// no new blocks to work on

		// Commit the transaction.
		if err = tx.Commit(); err != nil {
			log.Fatal(err)
		}

		// respond to message with result (empty block)
		j, _ := json.Marshal(b)
		m.Respond(j)

		return
	}

	// load up the 'root' block bignum size
	dbRootSize, err := dbbig.Load(db, root.SizeId)
	if err != nil {
		log.Println(err)
		return
	}
	// create new bignum using 'index' value from the root block
	dbBlockIndex, err := dbbig.New(db, dbRootIndex.Value.String())
	if err != nil {
		log.Println(err)
		return
	}
	// save to database
	dbBlockIndex.Save()
	// create new bignum using 'size' value from the root block
	dbBlockSize, err := dbbig.New(db, dbRootSize.Value.String())
	if err != nil {
		log.Println(err)
		return
	}

	// if last block and size goes over total, reduce block size
	test := big.NewInt(0)
	test.Add(dbBlockIndex.Value, dbBlockSize.Value)
	if test.Cmp(dbListTotal.Value) > 0 {
		// reduce block size to avoid going over total
		dbBlockSize.Value.Sub(dbListTotal.Value, dbBlockIndex.Value)
	}
	// save to database
	dbBlockSize.Save()

	// add new block
	err = tx.QueryRowContext(ctx, `INSERT INTO block ("list", "state", "indexId", "sizeId", "replicas", "when", "who", "key") VALUES ($1, $2, $3, $4, $5, CURRENT_TIMESTAMP, $6, $7) RETURNING id;`,
		list.Id, "active", dbBlockIndex.GetId(), dbBlockSize.GetId(), 1, request.Who, key).Scan(&rowId)
	if err != nil {
		log.Println(err)
		return
	}

	// advance the root block 'index' forward the size of the block we handed out
	// - we don't update the 'block', but rather the big we have a reference to
	log.Printf("[get /block] Update root block index, prepping for next block\n")
	log.Printf("[get /block] 'root' block index (before) is: %v\n", dbRootIndex.Value.String())

	// add 'size' to index
	dbRootIndex.Add(&dbRootSize)
	// save updated index
	dbRootIndex.Save()
	log.Printf("[get /block] 'root' block index ( after) is: %v\n", dbRootIndex.Value.String())

	// load up the new block and return it
	// (TODO) we have the values before, so there's no need for this extra db query
	err = tx.QueryRowContext(ctx, `SELECT * FROM block WHERE "id"=$1`, rowId).Scan(
		&b.Id, &b.List, &b.State, &b.IndexId, &b.SizeId, &b.Replicas, &b.When, &b.Who, &b.Key)
	if err != nil {
		log.Println(err)
		return
	}

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Fatal(err)
	}

	// respond to message with result
	j, _ := json.Marshal(b)
	m.Respond(j)
}
