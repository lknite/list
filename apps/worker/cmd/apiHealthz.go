package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/lib/pq"
)

func HealthInit() {
	HealthInitHandles()
}

func HealthInitHandles() {

	// start up health checks on their own thread
	wg.Add(1)
	go func() {
		defer wg.Done()

		// create health check endpoint
		http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			// test sql connection
			rows, err := db.Query(`SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'`)
			if err != nil {
				//log.Fatalln(err)
				w.WriteHeader(500)
				return
			}
			defer rows.Close()

			// if no errors, return statuscode 200
			w.Write([]byte("ok"))
		})

		// create a 'server' so we can later use 'shutdown'
		srv := &http.Server{
			Addr: ":8081",
		}

		// handle SIGINT / SIGTERM
		go func() {
			sigint := make(chan os.Signal)
			signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
			<-sigint

			log.Println("- shutdownHealthzHandler")

			// cancel routine during shutdown, if needed
			sigctx, sigcancel := context.WithTimeout(context.Background(), 20*time.Second)
			defer func() {
				// extra handling here
				sigcancel()
			}()

			// stop listening, finish calls in progress, then exit
			if err := srv.Shutdown(sigctx); err != nil {
				log.Fatalf("- shutdownHealthzHandler: %+v", err)
			}
			log.Print("- shutdownHealthzHandler exited")
		}()

		// begin handling connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
}
