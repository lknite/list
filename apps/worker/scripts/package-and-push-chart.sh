#!/bin/sh


# check current working directory
if [ ! -f "./$(basename $0)" ]; then
  echo "abort, change to scripts folder before executing"
  exit 1
fi

# check environment variables (in a /bin/sh compatible way)
read -r -d '' ITEMS <<EOF
PROJECT
REPO_CHARTS_USERNAME
REPO_CHARTS_PASSWORD
REPO_CHARTS_FQDN
EOF
for next in $ITEMS; do
  echo "checking for required env vars: $next"
  value=$(eval "echo \$$next")
  if [ -z $value ]; then
    echo "abort, required environment variable not present: '${next}'"
    exit 1
  fi
done

# container version may be provided via environment variable or via ../tag-container file
if [ -z $CONTAINER_VERSION ]; then
  # generate next container version
  CONTAINER_VERSION=`echo "$(cat ../build/ver-MAJOR).$(cat ../build/ver-MINOR).$(cat ../build/ver-PATCH)-$(echo $CI_PIPELINE_IID)"`
fi

# chart version may be provided via environment variable or via ../tag-helm-chart file
if [ -z $CHART_VERSION ]; then
  # generate next chart version
  CHART_VERSION=`echo "$(cat ../build/ver-MAJOR).$(cat ../build/ver-MINOR).$(cat ../build/ver-PATCH)-$(echo $CI_PIPELINE_IID)"`
fi


# adjust folder to work out of
cd ..


# overwrite the chart version for the packaging process
sed -i "s/^name:.*/name: $APP/g" chart/Chart.yaml
sed -i "s/^version:.*/version: ${CHART_VERSION}/g" chart/Chart.yaml
sed -i "s/^  tag:.*/  tag: ${CONTAINER_VERSION}/g" chart/values.yaml
sed -i "s|^  repository:.*|  repository: ${REPO_CHARTS_FQDN}/${PROJECT}|g" chart/values.yaml

# debug
cat chart/Chart.yaml
cat chart/values.yaml

# package up helm chart (will use the increased patch version)
echo helm package chart --dependency-update
helm package chart --dependency-update


# acquire chart name and version, these will be used when pushing to repo
CHART_NAME=$(grep "^name:" chart/Chart.yaml | cut -d ' ' -f 2)
echo "chart name: ${CHART_NAME}"
CHART_VERSION=$(grep "^version:" chart/Chart.yaml | cut -d ' ' -f 2)
echo "chart version: ${CHART_VERSION}"

# login to repo
echo helm registry login ${REPO_CHARTS_FQDN} -u "${REPO_CHARTS_USERNAME}" --password-stdin
echo ${REPO_CHARTS_PASSWORD} | helm registry login ${REPO_CHARTS_FQDN} -u "${REPO_CHARTS_USERNAME}" --password-stdin

# push helm chart to repo
echo helm push "${CHART_NAME}-${CHART_VERSION}.tgz" "${REPO_CHARTS_FQDN}/${PROJECT}-charts"
helm push "${CHART_NAME}-${CHART_VERSION}.tgz" "oci://${REPO_CHARTS_FQDN}/${PROJECT}-charts"
