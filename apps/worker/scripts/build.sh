#!/bin/sh


# check current working directory
if [ ! -f "./$(basename $0)" ]; then
  echo "abort, change to scripts folder before executing"
  exit 1
fi


# adjust folder to work out of
cd ../cmd

# disable dynamic linking, instead use static linking for a standalone binary
#CGO_ENABLED=0

go mod tidy
CGO_ENABLED=0 go build -v -o app
