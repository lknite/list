#!/bin/sh


# check current working directory
if [ ! -f "./$(basename $0)" ]; then
  echo "abort, change to scripts folder before executing"
  exit 1
fi

# check environment variables (in a /bin/sh compatible way)
read -r -d '' ITEMS <<EOF
PROJECT
REPO_CHARTS_FQDN
EOF
for next in $ITEMS; do
  echo "checking for required env vars: $next"
  value=$(eval "echo \$$next")
  if [ -z $value ]; then
    echo "abort, required environment variable not present: '${next}'"
    exit 1
  fi
done


# adjust folder to work out of
cd ..

# get most recent helm chart version via repo
CHART_VERSION=$(helm show chart oci://${REPO_CHARTS_FQDN}/${PROJECT}-charts/${PROJECT} | grep '^version' | cut -d ' ' -f 2)

# quick check if this is the first helm chart item and set a version if so
if [ -z $CHART_VERSION ]; then
  CHART_VERSION=0.0.0
fi

# parse off the major, minor, and patch
MAJOR=$(echo ${CHART_VERSION} | cut -d '.' -f 1)
MINOR=$(echo ${CHART_VERSION} | cut -d '.' -f 2)
PATCH=$(echo ${CHART_VERSION} | cut -d '.' -f 3)
#echo "major: ${MAJOR}"
#echo "minor: ${MINOR}"
#echo "patch: ${PATCH}"

# increase the 'patch' version
let "PATCH=PATCH+1"
#echo "patch: $PATCH"
CHART_VERSION="${MAJOR}.${MINOR}.${PATCH}"
echo "version: ${CHART_VERSION}"

echo "${CHART_VERSION}" > tag
