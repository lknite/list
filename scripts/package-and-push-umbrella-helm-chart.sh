#!/bin/sh


# check current working directory
if [ ! -f "./$(basename $0)" ]; then
  echo "abort, change to scripts folder before executing"
  exit 1
fi

# check environment variables (in a /bin/sh compatible way)
read -r -d '' ITEMS <<EOF
PROJECT
REPO_CHARTS_USERNAME
REPO_CHARTS_PASSWORD
REPO_CHARTS_FQDN
EOF
for next in $ITEMS; do
  echo "checking for required env vars: $next"
  value=$(eval "echo \$$next")
  if [ -z $value ]; then
    echo "abort, required environment variable not present: '${next}'"
    exit 1
  fi
done

# chart version may be provided via environment variable or via ../tag file
if [ -z $CHART_VERSION ]; then
  # get chart version from previous pipeline build step
  CHART_VERSION=$(cat ../tag)

  # if still empty, abort
  if [ -z $CHART_VERSION ]; then
    echo "abort, chart_version missing"
    exit 1
  fi
fi


# adjust folder to work out of
cd ..


# overwrite the chart version for the packaging process
sed -i "s/^name:.*/name: ${PROJECT}/g" chart/Chart.yaml
sed -i "s/^version:.*/version: ${CHART_VERSION}/g" chart/Chart.yaml


# loop through all apps adding as dependencies (skipping folder with project name)
cd apps

# loop through apps
for app in $(ls -d */); do
  # chop off trailing '/'
  app=${app:0:${#app} - 1}

  # ignore $PROJECT folder, which is not a microservice on its own
  if [ $app == $PROJECT ]; then
    continue
  fi

  # lookup latest chart version of this app
  CHART_VERSION=$(helm show chart oci://${REPO_CHARTS_FQDN}/${PROJECT}-charts/${app} --devel | grep '^version' | cut -d ' ' -f 2)

  # append this app as a dependency
  echo "- name: ${app}" >> ../chart/Chart.yaml
  echo "  version: ${CHART_VERSION}" >> ../chart/Chart.yaml
  echo "  repository: oci://${REPO_CHARTS_FQDN}/${PROJECT}-charts" >> ../chart/Chart.yaml
done

#
cd ..


# package up helm chart (will use the increased patch version)
echo helm package chart --dependency-update
helm package chart --dependency-update


# acquire chart name and version, these will be used when pushing to repo
CHART_NAME=$(grep "^name:" chart/Chart.yaml | cut -d ' ' -f 2)
echo "chart name: ${CHART_NAME}"
CHART_VERSION=$(grep "^version:" chart/Chart.yaml | cut -d ' ' -f 2)
echo "chart version: ${CHART_VERSION}"

# login to repo
echo helm registry login ${REPO_CHARTS_FQDN} -u "${REPO_CHARTS_USERNAME}" --password-stdin
echo ${REPO_CHARTS_PASSWORD} | helm registry login ${REPO_CHARTS_FQDN} -u "${REPO_CHARTS_USERNAME}" --password-stdin

# push helm chart to repo
echo helm push "${CHART_NAME}-${CHART_VERSION}.tgz" "${REPO_CHARTS_FQDN}/${PROJECT}-charts"
helm push "${CHART_NAME}-${CHART_VERSION}.tgz" "oci://${REPO_CHARTS_FQDN}/${PROJECT}-charts"
