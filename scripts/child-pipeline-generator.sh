#!/bin/sh


# not needed within a pipeline, but needed if run stand-alone
echo '' > child-pipeline-gitlab-ci.yml

# loop through apps appending child pipelines for each
ls apps | grep -v "trigger" | while IFS= read -r APP; do

cat >> child-pipeline-gitlab-ci.yml << EndOfString
${APP}:
  variables:
    APP: ${APP}
  trigger:
    include:
    - local: '/apps/${APP}/.gitlab-ci.yml'
    strategy: depend
  rules:
    - changes:
      - apps/${APP}/**/*
      allow_failure: true

EndOfString

done
