# architecture

## horizontally scalable workflow
- api methods generate message for (exactly one) subscriber, in group of subscribers, and wait for a response
  - message subscriber recieves message
    - message subscriber performs work
    - message subscriber replies with work complete
  - api methods respond as appropriate depending on result of work

## common files
| file                    | desc                                                       |
| ----------------------- | ---------------------------------------------------------- |
| main.go                 | main                                                       |
| auth.go                 | middleware for http bearer token and apikey handling       |
| init*.go                | main init routines, initialize database, nats, etc...      |
| apiHealthz.go           | http kubernetes health check                               |
| apiKey.go               | REST api methods related to apiKeys                        |
| sqlKey.go               | sql table definition for apiKeys                           |
| startApiHandlers.go     | minimum of one http handler needed to process apiKeys      |
| startSubscribers.go     | minimum of one nats subscriber for processing apiKeys      |
| startControllers.go     | empty by default, used to start up kubernetes controllers  |
 
## files by naming convention
| file      | desc                                                       |
| --------- | ---------------------------------------------------------- |
| api*.go   | http rest api handlers                                     |
| ctl*.go   | kubernetes controllers, if implemented                     |
| sql*.go   | sql table definitions                                      |
| sub*.go   | nats subscribers, nats messages sent via rest api          |
