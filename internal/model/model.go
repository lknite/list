package model

import (
	"net/url"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

// NATS msg passed to controllers of work to do
type Request struct {
	When   time.Time     `json:"when"`
	Who    string        `json:"who"`
	Values url.Values    `json:"values"`
	Data   []byte        `json:"data"`
	Claims jwt.MapClaims `json:"claims"`
}

/*
// NATS response back from controllers upon work completion
type Response struct {
	RequestId int `json:"requestId"`
}
*/

// Big & []BigPart, mapped via database query
type BigPart struct {
	Index int    `json:"index"`
	Part  string `json:"part"`
	Hash  string `json:"hash"`
}
type Big struct {
	N     int       `json:"n"`
	M     int       `json:"m"`
	R     int       `json:"r"`
	Parts []BigPart `json:"parts"`
}

// ListRef, mapped via database query, uses references to Bigs
type ListRef struct {
	Id      int       `json:"id"`
	When    time.Time `json:"when"`
	Who     string    `json:"who"`
	Name    string    `json:"name"`
	Task    string    `json:"task"`
	Action  string    `json:"action"`
	State   string    `json:"state"`
	Access  string    `json:"access"`
	TotalId int       `json:"totalId"`
	SizeId  int       `json:"sizeId"`
}

// List, same as ListRef but with Bigs defined
type List struct {
	Id     int       `json:"id"`
	When   time.Time `json:"when"`
	Who    string    `json:"who"`
	Name   string    `json:"name"`
	Task   string    `json:"task"`
	Action string    `json:"action"`
	State  string    `json:"state"`
	Access string    `json:"access"`
	Total  Big       `json:"total"`
	Size   Big       `json:"size"`
}

// BlockRef, mapped via database query, uses references to Bigs
type BlockRef struct {
	Id       int       `json:"id"`
	List     int       `json:"list"`
	State    string    `json:"state"`
	IndexId  int       `json:"indexId"`
	SizeId   int       `json:"sizeId"`
	Replicas int       `json:"replicas"`
	When     time.Time `json:"when"`
	Who      string    `json:"who"`
	Key      uuid.UUID `json:"key"`
}

// Block, same as BlockRef but with Bigs defined
type Block struct {
	Id       int       `json:"id"`
	List     int       `json:"list"`
	State    string    `json:"state"`
	Index    Big       `json:"index"`
	Size     Big       `json:"size"`
	Replicas int       `json:"replicas"`
	When     time.Time `json:"when"`
	Who      string    `json:"who"`
	Key      uuid.UUID `json:"key"`
}

// KeyRef
type KeyRef struct {
	Id     int    `json:"id"`
	Who    string `json:"who"`
	Hash   string `json:"hash"`
	Exp    int    `json:"exp"`
	Name   string `json:"name"`
	Claims string `json:"claims"`
}

/*
// Key, same as KeyRef but with values decrypted
type Key struct {
	Id     int    `json:"id"`
	Who    string `json:"who"`
	Hash   string `json:"hash"`
	Exp    int    `json:"exp"`
	Name   string `json:"name"`
	Claims string `json:"claims"`
}
*/
