module gitlab.com/lknite/list/internal/dbbig

go 1.22.1

require (
	github.com/nats-io/nats.go v1.35.0
	gitlab.com/lknite/list/internal/model v0.0.0-unpublished
)

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/nats-io/nkeys v0.4.7 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
)

replace gitlab.com/lknite/list/internal/model v0.0.0-unpublished => ../model
