package dbbig

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"math"
	"math/big"

	"github.com/nats-io/nats.go"

	"gitlab.com/lknite/list/internal/model"
)

/*
type DbBig interface {
	New(db *sql.DB, value string) dbBig
	Load(db *sql.DB, id int) dbBig
	Save()
}
*/

type DbBig struct {
	db *sql.DB
	//tx    *sql.Tx
	//ctx   nats.ContextOpt
	id    int
	Value *big.Int
	big   model.Big
}

func (b *DbBig) GetId() (id int) {
	// if 'New' was used, and 'Save' has not been called, then .Id will not yet exist
	if b.id == 0 {
		// save to database, this will cause the .Id to appear
		b.Save()
	}

	return b.id
}

func (b *DbBig) Add(num *DbBig) {
	log.Printf("before: %v, num to add: %v\n", b.Value.String(), num.Value.String())
	b.Value.Add(b.Value, num.Value)
	log.Printf(" after: %v\n", b.Value.String())
}

func Load(db *sql.DB /*, tx *sql.Tx, ctx nats.ContextOpt*/, id int) (DbBig, error) {
	//log.Printf("dbbig Load\n")

	// instantiate a new dbBig
	var b DbBig
	// set database connection
	b.db = db
	// set big rowId
	b.id = id

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	/*
		// get big metadata
		err := b.Db.QueryRowContext(ctx, `SELECT "n", "m", "r" FROM big WHERE "id"=$1`, id).Scan(&b.big.N, &b.big.M, &b.big.R)
		if err != nil {
			log.Println(err)
			return b, err
		}
	*/

	// get big parts
	rows, err := b.db.QueryContext(ctx, `SELECT "index", "part", "hash" FROM big_part WHERE "bigId"=$1;`, id)
	if err != nil {
		log.Println(err)
		return b, err
	}
	defer rows.Close()

	// loop through rows appending to big struct
	var value string
	for rows.Next() {
		var p model.BigPart

		err = rows.Scan(&p.Index, &p.Part, &p.Hash)
		if err != nil {
			log.Println(err)
			return b, err
		}

		value += p.Part
		//b.big.Parts = append(b.big.Parts, p)
	}

	// create big.Int from value string
	var success bool
	b.Value, success = (new(big.Int)).SetString(value, 10)
	if !success {
		return b, errors.New("dbbig load: (new(big.Int)).SetString failed")
	}

	return b, nil
}

func New(db *sql.DB /*, tx *sql.Tx, ctx nats.ContextOpt*/, value string) (DbBig, error) {
	log.Printf("dbbig New(%v)\n", value)

	// instantiate a new dbBig
	var b DbBig
	// set database connection
	b.db = db

	// create big.Int from value string
	var success bool
	b.Value, success = (new(big.Int)).SetString(value, 10)
	if !success {
		return b, errors.New("dbbig new: (new(big.Int)).SetString failed")
	}

	log.Printf("dbbig New, b.Value: %v\n", b.Value.String())
	return b, nil
}

func (b *DbBig) Save() error {
	log.Printf("dbbig Save\n")

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return err
	}
	defer tx.Rollback()

	// get Big struct using big.Int as the source
	b.big = itob(b.Value)

	// if this is a new dbbig
	if b.id == 0 {
		// insert big metadata

		log.Printf("dbbig INSERT\n")
		err = tx.QueryRowContext(ctx, `INSERT INTO big ("n", "m", "r") VALUES ($1, $2, $3) RETURNING id;`, b.big.N, b.big.M, b.big.R).Scan(&b.id)
		if err != nil {
			log.Println(err)
			return err
		}

		// insert big parts
		for _, part := range b.big.Parts {
			_, err = tx.ExecContext(ctx, `INSERT INTO big_part ("bigId", "index", "part", "hash") VALUES ($1, $2, $3, $4);`,
				b.id, part.Index, part.Part, part.Hash)
			if err != nil {
				log.Println(err)
				return err
			}
		}
	} else {
		// otherwise, update existing

		log.Printf("dbbig UPDATE\n")
		_, err = tx.ExecContext(ctx, `UPDATE big SET "n"=$1,"m"=$2,"r"=$3 WHERE id=$4;`, b.big.N, b.big.M, b.big.R, b.id)
		if err != nil {
			log.Println(err)
			return err
		}

		// insert big parts
		for i, part := range b.big.Parts {
			res, err := tx.ExecContext(ctx, `UPDATE big_part SET "part"=$1,"hash"=$2 WHERE "bigId"=$3 AND "index"=$4;`,
				part.Part, part.Hash, b.id, i)
			if err != nil {
				log.Println(err)
				return err
			}

			// check if no rows were effected, if so we will need to use INSERT instead of UPDATE
			n, err := res.RowsAffected()
			if err != nil {
				log.Println(err)
				return err
			}

			// if an additional row is needed the UPDATE will effect 0 rows, use an INSERT instead
			if n == 0 {
				log.Printf("dbbig update: additional row needed, using INSERT\n")
				_, err = tx.ExecContext(ctx, `INSERT INTO big_part ("bigId", "index", "part", "hash") VALUES ($1, $2, $3, $4);`,
					b.id, i, part.Part, part.Hash)
				if err != nil {
					log.Println(err)
					return err
				}
			}
		}
	}

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return err
	}

	log.Printf("dbbig Save, id: %d\n", b.id)
	return nil
}

func (b *DbBig) Delete() error {
	log.Printf("dbbig Delete\n")

	// get context to use with transaction
	ctx := nats.Context(context.Background())

	// Get a Tx for making transaction requests.
	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		log.Println(err)
		return err
	}
	defer tx.Rollback()

	log.Printf("dbbig DELETE\n")
	_, err = tx.ExecContext(ctx, `DELETE FROM big_part WHERE "bigId"=$1;`, b.id)
	if err != nil {
		log.Println(err)
		return err
	}
	_, err = tx.ExecContext(ctx, `DELETE FROM big WHERE "id"=$1;`, b.id)
	if err != nil {
		log.Println(err)
		return err
	}

	// Commit the transaction.
	if err = tx.Commit(); err != nil {
		log.Println(err)
		return err
	}

	log.Printf("dbbig Delete, id: %d\n", b.id)
	return nil
}

/*
// Get 'Big' as a Bignum 'Int'
func btoi(b model.Big) (z *big.Int, success bool) {
	var tmp string

	//log.Printf("Btoi\n")
	for _, part := range b.Parts {
		//log.Printf("  append '%v'\n", part.Part)
		tmp += part.Part
	}

	//log.Printf("  z.SetString: '%v'\n", tmp)
	n := new(big.Int)
	z, success = n.SetString(tmp, 10)

	return z, success
}
*/

/**
 * Bignum 'Int' to 'Big'
 * m is the max digits in a part, a row stored in the database
 * n is the number of rows which are of 'max' size, there may be one more row if there is a remainder
 * r is the remainder, if there is an additional row with less digits than 'max'
 */
func itob(z *big.Int) (b model.Big) {
	// get Bignum Int as a string for easy parsing
	str := z.String()

	// calculate how many parts (records in the database) will be needed to store the bigint
	total := len(str)
	b.M = 4

	// var parts float64
	quotient := math.Floor(float64(total) / float64(b.M))
	remainder := math.Ceil(float64(total) / float64(b.M))
	b.N = int(quotient)
	if quotient != remainder {
		b.R = total - (b.N * b.M)
	}
	/*
		log.Printf("Itob: n: %d\n", b.N)
		log.Printf("Itob: m: %d\n", b.M)
		log.Printf("Itob: r: %d\n", b.R)
	*/

	// split up the bigint into parts
	//log.Printf("Itob: split up the bigint into parts and insert as a record")
	ptr := 0
	index := 0
	for {
		// add each part to Big
		if total < (ptr + b.M) {
			//log.Printf("Itob: last: %v\n", str[ptr:ptr+(total-ptr)])
			var part model.BigPart
			part.Index = index
			part.Part = str[ptr : ptr+(total-ptr)]
			part.Hash = "???"
			b.Parts = append(b.Parts, part)

			ptr += (total - ptr)
		} else {
			//log.Printf("Itob: next: %v\n", str[ptr:ptr+b.M])
			var part model.BigPart
			part.Index = index
			part.Part = str[ptr : ptr+b.M]
			part.Hash = "???"
			b.Parts = append(b.Parts, part)

			ptr += b.M
		}

		// advance index
		index = index + 1

		// exit loop condition
		if ptr == total {
			break
		}
	}

	return b
}
